from .base import *

DATABASES = {
    "default": {
        "ENGINE": 'django.db.backends.postgresql_psycopg2',
        "NAME": 'cleaner',
        "USER": 'myprojectuser',
        "PASSWORD": 'myprojectuser',
        "HOST": 'localhost',
        "PORT":  '5432'
    }
}

CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
    'http://localhost:8000',
)
