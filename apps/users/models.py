import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser

from apps.users.constants import ROLE_CHOICES, GENDER_CHOICES
from apps.users.manager import UserManager
from apps.common.models.abstract import BaseModel


# Create your models here.


class User(AbstractUser, BaseModel):
    """ User model that stores role for registered user """

    username = models.CharField(
        'username',
        max_length=150,
        unique=True,
        help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.',
        error_messages={
            'unique': "A user with that username already exists.",
        },
        default=uuid.uuid4
    )

    email = models.EmailField(
        'email address', null=True,
        unique=True,
        error_messages={
            'unique': "A user with that email already exists.",
        }
    )

    role = models.CharField(max_length=25, choices=ROLE_CHOICES)

    is_blocked = models.BooleanField(default=False)

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return "{}".format(self.email)
