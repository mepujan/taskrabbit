from django.db import transaction
from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.password_validation import validate_password as validate_dpassword

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler

from apps.api.mixins.serializers import DynamicFieldsModelSerializer, DummySerializer
from apps.api.mixins.utils import DummyObject
from apps.taskers.models import Tasker
from apps.users.manager import UserManager
from apps.users.constants import TASKER, COHOST
from apps.cohosts.models import CoHost

USER = get_user_model()


class UserDetailSerializer(DynamicFieldsModelSerializer):
    tasker_id = serializers.SerializerMethodField()
    cohost_id = serializers.SerializerMethodField()

    class Meta:
        model = USER
        fields = (
            'id',
            'email',
            'role',
            'first_name',
            'last_name',
            'is_blocked',
            'tasker_id',
            'cohost_id'
        )

    def get_tasker_id(self, obj):
        if hasattr(obj, 'tasker'):
            return obj.tasker.id
        return None

    def get_cohost_id(self, obj):
        if hasattr(obj, 'cohost'):
            return obj.cohost.id
        return None


class UserRegisterSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = USER
        fields = ('email', 'password')
        extra_kwargs = {
            'email': {
                'required': True
            }
        }

    def validate_password(self, value):
        validate_dpassword(value)
        return value

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        return validated_data

    def save_user(self, validated_data):
        email = validated_data.get('email')
        password = validated_data.get('password')
        role = validated_data.get('role')

        user = USER(email=email)
        user.set_password(password)
        user.role = role
        user.save()

        return user


class TaskerRegisterSerializer(UserRegisterSerializer):

    def create(self, validated_data):
        with transaction.atomic():
            validated_data['role'] = TASKER
            user = self.save_user(validated_data)
            Tasker.objects.create(user=user)

        return DummyObject(**validated_data)


class CoHostRegisterSerializer(UserRegisterSerializer):

    def create(self, validated_data):
        with transaction.atomic():
            validated_data['role'] = COHOST
            user = self.save_user(validated_data)
            CoHost.objects.create(user=user)

        return DummyObject(**validated_data)
