from django.contrib.auth import authenticate, get_user_model
from django.utils.translation import gettext as _
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework_jwt.compat import Serializer, PasswordField
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler


USER = get_user_model()


class CustomJSONWebTokenSerializer(Serializer):
    email = serializers.EmailField()
    password = PasswordField(write_only=True)

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        credentials = {
            'email': email,
            'password': password
        }

        if all(credentials.values()):
            user = authenticate(**credentials)

            if user:
                if not user.is_active:
                    msg = 'User account is disabled.'
                    raise serializers.ValidationError({"message": msg})

                if user.is_blocked:
                    msg = 'Something went wrong, Please contact customer support for further detail!'
                    raise serializers.ValidationError({"message": msg})

                payload = jwt_payload_handler(user)

                return {
                    'token': jwt_encode_handler(payload),
                    'user': user
                }
            else:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError({"message": msg})
        else:
            msg = 'Must include "Phone Number" and "password".'
            raise serializers.ValidationError({"message": msg})
