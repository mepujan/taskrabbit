from django.contrib.auth import get_user_model, authenticate
from rest_framework.response import Response
from rest_framework_jwt.views import ObtainJSONWebToken
from apps.users.api.serializers.auth import CustomJSONWebTokenSerializer
from apps.users.api.serializers.user import UserDetailSerializer


USER = get_user_model()


class CustomObtainJSONWebToken(ObtainJSONWebToken):
    serializer_class = CustomJSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)

        credentials = {
            'email': request.POST.get('email'),
            'password': request.POST.get('password')
        }

        user = authenticate(**credentials)
        if user:
            serializer = UserDetailSerializer(user)
            response.data['user'] = serializer.data

        return response
