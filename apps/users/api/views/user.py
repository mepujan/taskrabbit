from django.contrib.auth import get_user_model

from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.api.mixins.viewsets import CreateRetrieveViewSet
from apps.users.api.serializers import (
    UserDetailSerializer,
    TaskerRegisterSerializer,
    CoHostRegisterSerializer
)

USER = get_user_model()


class UserViewSet(CreateRetrieveViewSet):
    serializer_class = UserDetailSerializer
    queryset = USER.objects.filter(
        is_blocked=False,
        is_active=True
    )
    permission_classes = [IsAuthenticated, ]

    def get_object(self):
        return self.request.user

    def check_permissions(self, request):
        if self.action in ['create', 'retrieve']:
            self.permission_denied(request)
        return super().check_permissions(request)

    @action(
        detail=False,
        methods=['post', ],
        serializer_class=TaskerRegisterSerializer,
        permission_classes=[],
        url_path="register/tasker"
    )
    def register_tasker(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    @action(
        detail=False,
        methods=['post', ],
        serializer_class=CoHostRegisterSerializer,
        permission_classes=[],
        url_path="register/cohost"
    )
    def register_cohost(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    @action(
        detail=False,
        methods=['get', ],
        url_path="me"
    )
    def me(self, request, *args, **kwargs):
        user = request.user
        response = {}
        if user:
            serializer = UserDetailSerializer(user)
            response['user'] = serializer.data
        return Response(response)
