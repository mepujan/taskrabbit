from django.urls import path
from rest_framework_jwt.views import (
    refresh_jwt_token,
    verify_jwt_token
)

from apps.users.api.views.auth import CustomObtainJSONWebToken


app_name = 'auth'

urlpatterns = [
    path('get-token/', CustomObtainJSONWebToken.as_view(), name='get_token'),
    path('refresh-token/', refresh_jwt_token),
    path('verify-token/', verify_jwt_token),
]
