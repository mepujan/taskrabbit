from django.urls import path
from rest_framework import routers
from apps.users.api.views.user import UserViewSet

app_name = 'users'

router = routers.DefaultRouter()
router.register(r'', UserViewSet, basename='user')

urlpatterns = router.urls
