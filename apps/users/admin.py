from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as DjUserAdmin
from django.contrib.auth.forms import UserCreationForm as DjUserCreateForm, UsernameField

USER = get_user_model()


class UserCreationForm(DjUserCreateForm):
    class Meta:
        model = USER
        fields = ("username", "email", "role")
        field_classes = {'username': UsernameField}


class UserAdmin(DjUserAdmin):
    add_form = UserCreationForm
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2', 'role'),
        }),
    )
    list_per_page = 10
    list_display = (
        'id',
        'email',
        'date_joined',
        'role'
    )


admin.site.register(USER, UserAdmin)
