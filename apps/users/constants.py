MALE, FEMALE, OTHER = 'Male', 'Female', 'Other'

GENDER_CHOICES = (
    (MALE, MALE),
    (FEMALE, FEMALE),
    (OTHER, OTHER),
)

TASKER, COHOST, HOST, ADMIN = 'Tasker', 'Cohost', 'Host', 'Admin'

ROLE_CHOICES = (
    (TASKER, TASKER),
    (COHOST, COHOST),
    (HOST, HOST),
    (ADMIN, ADMIN),
)
