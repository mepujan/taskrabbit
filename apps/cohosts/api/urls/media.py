from django.urls import path
from rest_framework.routers import DefaultRouter
from apps.cohosts.api.views import CoHostMediaViewSet

app_name = 'cohost_media'

router = DefaultRouter()

router.register(r'', CoHostMediaViewSet)

urlpatterns = router.urls
