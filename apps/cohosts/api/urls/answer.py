from django.urls import path
from rest_framework.routers import DefaultRouter
from apps.cohosts.api.views import CoHostQuestionaireAnswerViewSet

app_name = 'cohost_answer'

router = DefaultRouter()

router.register(r'', CoHostQuestionaireAnswerViewSet)

urlpatterns = router.urls
