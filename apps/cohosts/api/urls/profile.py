from django.urls import path
from rest_framework.routers import DefaultRouter
from apps.cohosts.api.views import CoHostProfileViewSet

app_name = 'cohost_profile'

router = DefaultRouter()

router.register(r'', CoHostProfileViewSet)

urlpatterns = router.urls
