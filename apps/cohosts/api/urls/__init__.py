from django.urls import path, include
urlpatterns = [

    path('profile/', include('apps.cohosts.api.urls.profile')),
    path('questions/', include('apps.cohosts.api.urls.questionaire')),
    path('answers/', include('apps.cohosts.api.urls.answer')),
    path('media/', include('apps.cohosts.api.urls.media')),

]
