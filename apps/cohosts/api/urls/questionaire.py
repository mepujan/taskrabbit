from django.urls import path
from rest_framework.routers import DefaultRouter
from apps.cohosts.api.views import CoHostQuestionaireList

app_name = 'cohost_question'

router = DefaultRouter()

router.register(r'', CoHostQuestionaireList)

urlpatterns = router.urls
