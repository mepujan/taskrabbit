from rest_framework import serializers
from apps.users.api.serializers.user import UserDetailSerializer
from apps.api.mixins.serializers import DynamicFieldsModelSerializer
from apps.cohosts.models import (CoHostProfile, CoHostQuestionaire,
                                 CoHostQuestionaireAnswer, CoHostSocialAccount)


class CoHostProfileSerializer(DynamicFieldsModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = CoHostProfile
        fields = (
            'id',
            'gender',
            'phone_number',
            'business_name',
            'address',
            'website',
            'hours',
            'cohost',
            'user'
        )
        extra_kwargs = {
            'phone_number': {
                'required': True
            },
            'business_name': {
                'required': True
            }
        }

    def get_user(self, obj):
        user = obj.cohost.user
        serializer = UserDetailSerializer(user)
        return serializer.data


class CoHostQuestionaireSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = CoHostQuestionaire
        fields = "__all__"


class CoHostQuestionaireAnswerSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = CoHostQuestionaireAnswer
        fields = "__all__"


class CoHostMediaSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = CoHostSocialAccount
        fields = "__all__"
