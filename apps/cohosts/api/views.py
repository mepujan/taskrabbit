from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from apps.api.mixins.viewsets import CreateListRetrieveUpdateViewSet, ListViewSet
from apps.cohosts.models import (
    CoHostProfile, CoHostQuestionaire, CoHostQuestionaireAnswer, CoHostSocialAccount)
from apps.cohosts.api.serializers import (
    CoHostProfileSerializer,
    CoHostQuestionaireSerializer,
    CoHostQuestionaireAnswerSerializer,
    CoHostMediaSerializer
)


class CoHostProfileViewSet(CreateListRetrieveUpdateViewSet):
    queryset = CoHostProfile.objects.all()
    serializer_class = CoHostProfileSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    search_fields = [
        'address',
        'phone_number',
        'website'
    ]
    filter_fields = [
        'cohost',
        'gender',
        'hours'
    ]
    ordering_fields = [
        'created_at',
        'updated_at',
    ]


class CoHostQuestionaireList(ListViewSet):
    queryset = CoHostQuestionaire.objects.all()
    serializer_class = CoHostQuestionaireSerializer


class CoHostQuestionaireAnswerViewSet(CreateListRetrieveUpdateViewSet):
    queryset = CoHostQuestionaireAnswer.objects.all()
    serializer_class = CoHostQuestionaireAnswerSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    search_fields = [
        'answer'
    ]
    filter_fields = [
        'cohost',
        'questionaire'
    ]
    ordering_fields = [
        'created_at',
        'updated_at',
    ]


class CoHostMediaViewSet(CreateListRetrieveUpdateViewSet):
    queryset = CoHostSocialAccount.objects.all()
    serializer_class = CoHostMediaSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    ordering_fields = [
        'created_at',
        'updated_at',
    ]
