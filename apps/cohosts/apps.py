from django.apps import AppConfig


class CohostsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.cohosts'
