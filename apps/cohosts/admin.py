from django.contrib import admin
from apps.cohosts.models import (
    CoHost,
    CoHostSocialAccount,
    CoHostProfile,
    CoHostQuestionaire,
    CoHostSocialAccount,
    CoHostQuestionaireAnswer,

)

# Register your models here.
admin.site.register([CoHost,
                     CoHostQuestionaire,
                     CoHostSocialAccount,
                     CoHostQuestionaireAnswer,
                     CoHostProfile,
                     ])
