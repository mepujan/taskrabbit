from django.db import models
from django.contrib.auth import get_user_model
from apps.common.models import (
    BaseModel,
    AbstractSocialAccount,
    AbstractUserProfile,
    AbstractQuestionaire
)

USER = get_user_model()


class CoHost(BaseModel):
    """ CoHost model  """

    user = models.OneToOneField(
        USER,
        on_delete=models.CASCADE,
        related_name='cohost'
    )

    class Meta:
        verbose_name = 'Co Host'
        verbose_name_plural = 'Co Hosts'

    def __str__(self):
        return self.user.email


class CoHostProfile(BaseModel, AbstractUserProfile):

    cohost = models.OneToOneField(
        CoHost,
        on_delete=models.CASCADE,
        related_name='profile'
    )

    def __str__(self):
        return self.tasker.user.email


class CoHostSocialAccount(BaseModel, AbstractSocialAccount):
    """ Tasker model  """

    cohost = models.ForeignKey(
        CoHost,
        on_delete=models.CASCADE,
        related_name='social_accounts'
    )

    class Meta:
        verbose_name = 'CoHost Social Account'
        verbose_name_plural = 'CoHost Social Accounts'

    def __str__(self):
        return self.cohost.user.email


class CoHostQuestionaire(BaseModel, AbstractQuestionaire):
    pass


class CoHostQuestionaireAnswer(BaseModel):
    """ Tasker Questionaire Answer model """

    cohost = models.ForeignKey(
        CoHost,
        related_name='answers',
        on_delete=models.CASCADE
    )

    questionaire = models.ForeignKey(
        CoHostQuestionaire,
        related_name="answers",
        on_delete=models.CASCADE
    )

    answer = models.CharField(max_length=225)

    def __str__(self):
        return self.cohost.user.email
