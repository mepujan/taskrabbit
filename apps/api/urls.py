from django.urls import path, include

app_name = "api_v1"

urlpatterns = [
    path('auth/', include('apps.users.api.urls.auth')),
    path('users/', include('apps.users.api.urls.user')),
    path('tasker/', include('apps.taskers.api.urls')),
    path('cohost/', include('apps.cohosts.api.urls')),
]
