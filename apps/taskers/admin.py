
from django.contrib import admin
from apps.taskers.models import (
    Tasker,
    TaskerSocialAccount,
    TaskerProfile,
    TaskerQuestionaire,
    TaskerSocialAccount,
    TaskerQuestionaireAnswer,

)

# Register your models here.
admin.site.register([Tasker,
                     TaskerQuestionaire,
                     TaskerSocialAccount,
                     TaskerQuestionaireAnswer,
                     TaskerProfile,
                     ])
