from rest_framework import serializers
from apps.taskers.models import (TaskerProfile, TaskerQuestionaire,
                                 TaskerQuestionaireAnswer, TaskerSocialAccount)
from apps.api.mixins.serializers import DynamicFieldsModelSerializer
from apps.users.api.serializers.user import UserDetailSerializer


class TaskerProfileSerializer(DynamicFieldsModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = TaskerProfile
        fields = (
            'id',
            'gender',
            'phone_number',
            'business_name',
            'address',
            'website',
            'hours',
            'tasker',
            'user'
        )
        extra_kwargs = {
            'phone_number': {
                'required': True
            },
            'business_name': {
                'required': True
            }
        }

    def get_user(self, obj):
        user = obj.tasker.user
        serializer = UserDetailSerializer(user)
        return serializer.data


class TaskerQuestionaireSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = TaskerQuestionaire
        fields = "__all__"


class TaskerQuestionaireAnswerSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = TaskerQuestionaireAnswer
        fields = "__all__"


class TaskerMediaSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = TaskerSocialAccount
        fields = "__all__"
