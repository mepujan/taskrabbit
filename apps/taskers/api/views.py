from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from apps.api.mixins.viewsets import CreateListRetrieveUpdateViewSet, ListViewSet
from apps.taskers.models import (TaskerProfile, TaskerQuestionaire,
                                 TaskerQuestionaireAnswer, TaskerSocialAccount)
from apps.taskers.api.serializers import (
    TaskerProfileSerializer,
    TaskerQuestionaireSerializer,
    TaskerQuestionaireAnswerSerializer,
    TaskerMediaSerializer
)


class TaskerProfileViewSet(CreateListRetrieveUpdateViewSet):
    queryset = TaskerProfile.objects.all()
    serializer_class = TaskerProfileSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    search_fields = [
        'address',
        'phone_number',
        'website'
    ]
    filter_fields = [
        'tasker',
        'gender',
        'hours'
    ]
    ordering_fields = [
        'created_at',
        'updated_at',
    ]


class TaskerQuestionaireList(ListViewSet):
    queryset = TaskerQuestionaire.objects.all()
    serializer_class = TaskerQuestionaireSerializer


class TaskerQuestionaireAnswerViewSet(CreateListRetrieveUpdateViewSet):
    queryset = TaskerQuestionaireAnswer.objects.all()
    serializer_class = TaskerQuestionaireAnswerSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    search_fields = [
        'answer'
    ]
    filter_fields = [
        'tasker',
        'questionaire'
    ]
    ordering_fields = [
        'created_at',
        'updated_at',
    ]


class TaskerMediaViewSet(CreateListRetrieveUpdateViewSet):
    queryset = TaskerSocialAccount.objects.all()
    serializer_class = TaskerMediaSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    ordering_fields = [
        'created_at',
        'updated_at',
    ]
