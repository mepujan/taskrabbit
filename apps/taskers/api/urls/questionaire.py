from django.urls import path
from rest_framework.routers import DefaultRouter
from apps.taskers.api.views import TaskerQuestionaireList

app_name = 'tasker_question'

router = DefaultRouter()

router.register(r'', TaskerQuestionaireList)

urlpatterns = router.urls
