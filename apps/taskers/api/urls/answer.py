from django.urls import path
from rest_framework.routers import DefaultRouter
from apps.taskers.api.views import TaskerQuestionaireAnswerViewSet

app_name = 'tasker_answer'

router = DefaultRouter()

router.register(r'', TaskerQuestionaireAnswerViewSet)

urlpatterns = router.urls
