from django.urls import path, include
urlpatterns = [

    path('profile/', include('apps.taskers.api.urls.profile')),
    path('questions/', include('apps.taskers.api.urls.questionaire')),
    path('answers/', include('apps.taskers.api.urls.answer')),
    path('media/', include('apps.taskers.api.urls.media')),

]
