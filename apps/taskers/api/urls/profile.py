from django.urls import path
from rest_framework.routers import DefaultRouter
from apps.taskers.api.views import TaskerProfileViewSet

app_name = 'tasker_profile'

router = DefaultRouter()

router.register(r'', TaskerProfileViewSet)

urlpatterns = router.urls
