from django.urls import path
from rest_framework.routers import DefaultRouter
from apps.taskers.api.views import TaskerMediaViewSet

app_name = 'tasker_media'

router = DefaultRouter()

router.register(r'', TaskerMediaViewSet)

urlpatterns = router.urls
