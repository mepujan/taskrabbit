from django.db import models
from django.contrib.auth import get_user_model
from apps.common.models import (
    AbstractSocialAccount, BaseModel,
    AbstractUserProfile,
    AbstractQuestionaire
)

USER = get_user_model()


class Tasker(BaseModel):
    """ Tasker model  """

    user = models.OneToOneField(
        USER,
        on_delete=models.CASCADE,
        related_name='tasker'
    )

    class Meta:
        verbose_name = 'Tasker'
        verbose_name_plural = 'Taskers'

    def __str__(self):
        return self.user.email


class TaskerProfile(BaseModel, AbstractUserProfile):

    tasker = models.OneToOneField(
        Tasker,
        on_delete=models.CASCADE,
        related_name='profile'
    )

    def __str__(self):
        return self.tasker.user.email


class TaskerSocialAccount(BaseModel, AbstractSocialAccount):
    """ Tasker model  """

    tasker = models.ForeignKey(
        Tasker,
        on_delete=models.CASCADE,
        related_name='social_accounts'
    )

    class Meta:
        verbose_name = 'Tasker Social Account'
        verbose_name_plural = 'Tasker Social Accounts'

    def __str__(self):
        return self.tasker.user.email


class TaskerQuestionaire(BaseModel, AbstractQuestionaire):
    pass


class TaskerQuestionaireAnswer(BaseModel):
    """ Tasker Questionaire Answer model """

    tasker = models.ForeignKey(
        Tasker,
        related_name='answers',
        on_delete=models.CASCADE
    )

    questionaire = models.ForeignKey(
        TaskerQuestionaire,
        related_name="answers",
        on_delete=models.CASCADE
    )

    answer = models.CharField(max_length=225)

    def __str__(self):
        return self.tasker.user.email
