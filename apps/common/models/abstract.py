from django.db import models
from apps.users.constants import GENDER_CHOICES


class BaseModel(models.Model):
    """Base model for this project."""

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class AbstractSocialAccount(models.Model):
    """
    Abstract Social Account model
    """

    account_name = models.CharField(max_length=100)
    url = models.URLField(max_length=255)

    def __str__(self):
        return self.account_name

    class Meta:
        abstract = True


class AbstractUserProfile(models.Model):
    """ Abstract Profile """

    profile_picture = models.ImageField(
        upload_to='pictures/', null=True, blank=True)
    gender = models.CharField(
        max_length=20,
        choices=GENDER_CHOICES,
        blank=True, null=True
    )

    phone_number = models.CharField(
        'phone number',
        null=True,
        max_length=25,
        error_messages={
            'unique': "A user with that phone number already exists.",
        },
        unique=True
    )
    business_name = models.CharField(
        'business name',
        null=True,
        max_length=125,
    )
    address = models.CharField(
        'address',
        null=True,
        max_length=225
    )
    website = models.URLField(max_length=25, null=True)
    hours = models.DecimalField(max_digits=19, decimal_places=2, null=True)

    class Meta:
        abstract = True


class AbstractQuestionaire(models.Model):
    """ Abstract Questionaire """
    question = models.TextField(max_length=1000)

    class Meta:
        abstract = True
